require 'pathname'
require 'set'

def ensureConstDefined symbol
  raise "A block must be given." \
    unless block_given?

  return if Object.const_defined? symbol

  Object.const_set symbol, yield
end

def ensurePathConstDefined symbol
  raise "A block must be given." \
    unless block_given?

  if Object.const_defined? symbol
    theValue = Object.const_get(symbol)

    case theValue
    when Pathname
      # No need to change a thing
    when String
      Object.send(:remove_const, symbol)
      Object.const_set symbol, Pathname::new(theValue)
    else
      raise "The type used for '#{symbol}' namely '#{theValue.class}' is not supported."
    end
  else
    Object.const_set symbol, Pathname::new(yield)
  end
end

def child? target, root
  target = Pathname::new(target).expand_path
  root = Pathname::new(root).expand_path

  target.to_s.start_with? root.to_s
end

ensureConstDefined :ContainerisationCmd do
  ENV.fetch 'CONTAINERISATION_CMD', 'docker'
end

ensureConstDefined(:IsPresentation) { false }

ensureConstDefined(:LaTeXImageName) { 'tianon/latex' }
ensureConstDefined(:SvgToPdfImageName) { 'webuni/inkscape:0.92' }

LaTeXContainerName = 'LaTeX-Rake-Container'
SvgToPdfContainerName = 'Inkscape-Rake-Container'

ensureConstDefined(:SvgToPdfStartingCommandList) { [] }
# Possible values:
# Inkscape=0.92.4
# Inkscape=1.1
ensureConstDefined(:SvgToPdfCommandSyntax) { 'Inkscape=0.92.4' }

OutDirectory = Pathname.new 'out'
OutDirectoryAbsolutePath = OutDirectory.expand_path
ProducedDirectory = Pathname.new 'produced'

ensurePathConstDefined :BaseDir do
  Pathname::new(__dir__) / '..'
end
BaseDirAbsolutePath = BaseDir.expand_path

ensurePathConstDefined :MainDir do
  BaseDir
end
MainDirAbsolutePath = MainDir.expand_path

raise "MainDir '#{MainDirAbsolutePath}' is not contained within BaseDir '#{BaseDirAbsolutePath}'." \
  unless child? MainDirAbsolutePath, BaseDirAbsolutePath

MainDirRelativeToBaseDir = MainDirAbsolutePath.relative_path_from BaseDirAbsolutePath

BaseDirAbsoluteInCompileContainer = '/source'
MainDirAbsoluteInCompileContainer = proc {
  relativePath = MainDirAbsolutePath.relative_path_from(BaseDirAbsolutePath).to_s

  if File::SEPARATOR == '\\'
    relativePath.gsub! /\\/, '/'
  end

  next "#{BaseDirAbsoluteInCompileContainer}/#{relativePath}"
}[]
OutDirAbsoluteInCompileContainer = "#{MainDirAbsoluteInCompileContainer}/#{OutDirectory}"

ensurePathConstDefined :BibInputDir do
  MainDirAbsolutePath
end
BibInputDirAbsolutePath = proc {
  if BibInputDir.absolute?
    next BibInputDir
  else
    next (MainDirAbsolutePath / BibInputDir).cleanpath
  end
}[]
BibInputDirAbsoluteInCompileContainer = [BaseDirAbsoluteInCompileContainer, (BibInputDirAbsolutePath.relative_path_from BaseDirAbsolutePath).to_s].join '/'

ensurePathConstDefined :BstInputDir do
  MainDirAbsolutePath
end
BstInputDirAbsolutePath = proc {
  if BstInputDir.absolute?
    next BstInputDir
  else
    next (MainDirAbsolutePath / BstInputDir).cleanpath
  end
}[]
BstInputDirAbsoluteInCompileContainer = [BaseDirAbsoluteInCompileContainer, (BstInputDirAbsolutePath.relative_path_from BaseDirAbsolutePath).to_s].join '/'

ensurePathConstDefined :ModeFolder do
  'modes'
end

ensurePathConstDefined :ModeBeforeFile do
  ModeFolder / 'currentModeBefore.tex'
end
ensurePathConstDefined :ModeFile do
  ModeFolder / 'currentMode.tex'
end

ensurePathConstDefined :MainTexFile do
  Pathname.new 'main.tex'
end
MainTexFileAbsolutePath = MainTexFile.expand_path

MainTexFileWithinCompileContainer = MainTexFileAbsolutePath.relative_path_from BaseDirAbsolutePath

OutFileName = Pathname.new 'main.pdf'
OutFile = OutDirectory / OutFileName

SvgExtension = '.svg'
PdfExtension = '.pdf'

ensurePathConstDefined :SvgPath do
  Pathname.new('images') / 'svgToPdf'
end
SvgAbsolutePath = SvgPath.expand_path
SvgAbsolutePathInsideContainer = '/images'

SvgPattern = SvgAbsolutePath / "*#{SvgExtension}"
PdfPattern = SvgAbsolutePath / "*#{PdfExtension}"

TexDirectories = Set.new(BaseDir.glob('**/*.tex').collect {
  |file| file.dirname
}).collect do
  |directory|
  # Make sure it is a relative path to the base directory
  directory.expand_path.relative_path_from BaseDirAbsolutePath
end

OutTexDirectories = TexDirectories.collect do
  |texDirectory| OutDirectory / texDirectory
end
