require 'open3'
require 'set'
require 'concurrent'

require_relative 'constants.rb'

def setOption inputMap, outputMap, symbol
  if inputMap.has_key? symbol
    outputMap[symbol] = inputMap[symbol]
  elsif block_given?
    outputMap[symbol] = yield
  end
end

def getContainerNamesFiltered regexFilter
  raise "Wrong type: '#{regexFilter.class}'" \
    unless String === regexFilter or Regex === regexFilter

  containerNames = []

  Open3.popen3 ContainerisationCmd, 'container', 'ls',
    '--quiet', '--filter', "name=#{regexFilter}" do
    |stdin, stdout, stderr, wait_thread|

    containerNames += stdout.readlines
  end

  return containerNames
end

def killContainerOnException containerName
  raise 'A container name must be provided.' \
    if containerName.nil? or containerName.size <= 0

  raise 'A block must be provided.' \
    unless block_given?

  begin
    yield
  rescue Exception => exception
    containerExists = true

    containerNames = getContainerNamesFiltered "^#{containerName}$"

    containerExists = if containerNames.length > 0
        true
      else
        false
      end

    system ContainerisationCmd, 'container', 'kill', containerName,
      exception: true \
      if containerExists

    raise
  end
end

def expandWithContainerName enumerator, containerNameBase
  unless enumerator.kind_of? Enumerable
    enumerator = [enumerator]
  end

  existingContainers = SortedSet::new(
    getContainerNamesFiltered("^#{containerNameBase}-").collect {|name| name.freeze}
  ).freeze

  containerNumberSuffix = 0

  return enumerator.collect do
    |original|

    containerName = nil

    loop do
      containerNumberSuffix += 1
      containerName = "#{containerNameBase}-#{containerNumberSuffix}"

      break unless existingContainers.include? containerName
    end

    next [original, containerName]
  end
end

def setupMode mode = nil
  unless mode.nil?
    mode = mode.downcase.to_sym
  else
    mode = :presentation
  end

  notesOnSecondMonitor = false
  handout = false

  case mode
  when :presentation
    notesOnSecondMonitor = false
    handout = false
  when :handout
    notesOnSecondMonitor = false
    handout = true
  when :notesaswell
    notesOnSecondMonitor = true
    handout = false
  else
    raise "The mode '#{mode}' is not supported."
  end

  File.open ModeFile, 'w' do
      |file|

      file << <<"THE_END"
\\toggle#{notesOnSecondMonitor ? 'true' : 'false'}{notesOnSecondMonitor}
THE_END
    end
    File.open ModeBeforeFile, 'w' do
      |file|

      file << <<"THE_END"
\\MySwitchHandout#{handout ? 'true' : 'false'}
THE_END
    end
end

def buildImages
  svgFiles = Dir[SvgPattern]
  svgFileToPdfMapping = Hash[svgFiles.map do
      |svgFile|

      raise "Not '#{SvgExtension}' at the end" unless svgFile.end_with? SvgExtension

      pdfFile = svgFile[0...-SvgExtension.size] + PdfExtension

      [svgFile, pdfFile]
    end
  ]

  def shouldBuild pdfFile, svgFile
    raise 'Wrong type' unless String === pdfFile
    raise 'Wrong type' unless String === svgFile
    raise 'Wrong content' if pdfFile.empty?
    raise 'Wrong content' if svgFile.empty?

    # File not present
    unless File.exist? pdfFile
      return true
    else
      # Compare modification times

      svgModifiedTime = File.mtime svgFile
      pdfModifiedTime = File.mtime pdfFile

      if svgModifiedTime > pdfModifiedTime
        return true
      else
        return false
      end
    end
  end

  svgFileNamesToBeBuiltToPdfMapping = {}

  svgFileToPdfMapping.each do |svgFile, pdfFile|
    if shouldBuild pdfFile, svgFile
      svgFileName = Pathname.new(svgFile).relative_path_from SvgAbsolutePath
      pdfFileName = Pathname.new(pdfFile).relative_path_from SvgAbsolutePath

      svgFileNamesToBeBuiltToPdfMapping[svgFileName] = pdfFileName
    end
  end

  threadPool = Concurrent::FixedThreadPool.new(Etc.nprocessors)

  executors = svgFileNamesToBeBuiltToPdfMapping.each_pair.then do
    |list|
    next expandWithContainerName list, SvgToPdfContainerName
  end.collect do
    |svgAndPdf, containerName|
    svgFileName, pdfFileName = *svgAndPdf

    next Concurrent::Future.execute({executor: threadPool}) do
      killContainerOnException containerName do
        command = [
          ContainerisationCmd, 'run',
          '--name', containerName,
          '--rm',
          '-v', "#{SvgAbsolutePath}:#{SvgAbsolutePathInsideContainer}",
          '-w', SvgAbsolutePathInsideContainer.to_s,
          SvgToPdfImageName,
          *SvgToPdfStartingCommandList,
        ]

        case SvgToPdfCommandSyntax
        when 'Inkscape=0.92.4'
          command.push(
            '--export-pdf=' + pdfFileName.to_s,
            svgFileName.to_s,
          )
        when 'Inkscape=1.1'
          command.push(
            '-o', pdfFileName.to_s,
            svgFileName.to_s,
          )
        else
          raise "'SvgToPdfCommandSyntax' is '#{SvgToPdfCommandSyntax}' which is not recognised."
        end

        system *command,
          exception: true
      end
    end
  end

  executors.map(&:wait!)
end

def buildLaTeXPdf
  escapedContainerInteralCommand = [
    'latexmk', "-outdir=#{OutDirAbsoluteInCompileContainer}", '-pdf', MainTexFileWithinCompileContainer
  ].collect { |argument| argument.to_s.dump }.join(' ')

  expandWithContainerName(nil, LaTeXContainerName).each do
    |unnecessary, containerName|

    killContainerOnException containerName do
      environmentVariables = []

      if BaseDirAbsolutePath != MainDirAbsolutePath
        environmentVariables += [
          "TEXINPUTS=#{MainDirRelativeToBaseDir}:",
          "BIBINPUTS=#{BibInputDirAbsoluteInCompileContainer}",
          "BSTINPUTS=#{BstInputDirAbsoluteInCompileContainer}",
        ]
      end

      environmentVariables = environmentVariables.collect do
        |env|

        next ['--env', env]
      end.flatten!

      command = [
        ContainerisationCmd, 'run',
        '--name', containerName,
        '--rm',
        '--mount', "type=bind,source=#{BaseDirAbsolutePath},destination=#{BaseDirAbsoluteInCompileContainer},readonly",
        '--mount', "type=bind,source=#{OutDirectoryAbsolutePath},destination=#{OutDirAbsoluteInCompileContainer}",
        '--workdir', BaseDirAbsoluteInCompileContainer,
        *environmentVariables,
        '--entrypoint', 'bash',
        LaTeXImageName,
        '-c', escapedContainerInteralCommand,
      ]

      system *command,
        exception: true
    end
  end
end

def ensureFileNotExisting file, **options
  optionsToBeUsed = {}
  setOption options, optionsToBeUsed, :verbose

  FileUtils.rm file.to_s, **optionsToBeUsed \
    if File.exist? file
end

def ensureDirNotExisting directory, **options
  optionsToBeUsed = {}
  setOption options, optionsToBeUsed, :verbose

  FileUtils.rm_rf directory.to_s, **optionsToBeUsed \
    if Dir.exist? directory
end
