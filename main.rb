require 'rake'
require 'pathname'
require 'fileutils'

require_relative 'constants.rb'
require_relative 'helpers.rb'

Dir.chdir MainDir

task default: :build

task :build, [:mode] => [OutDirectory.to_s, *OutTexDirectories.collect(&:to_s)] do
  |task, args|

  setupMode args.fetch :mode, nil \
    if IsPresentation

  buildImages
  buildLaTeXPdf
end

directory OutDirectory
for outTexDirectory in OutTexDirectories
  directory outTexDirectory
end

task :produce do
  Rake::Task[:clean].invoke 'all'
  Rake::Task[:produced].invoke

  def forMode mode
    Rake::Task[:build].reenable
    Rake::Task[:build].invoke mode
    FileUtils.cp \
      OutFile,
      "#{ProducedDirectory}/#{mode}.pdf"
  end

  if IsPresentation
    forMode :presentation
    forMode :handout
    forMode :notesAsWell
  else
    forMode :result
  end
end

directory ProducedDirectory

task :clean, [:all] do
  |task, args|

  ensureDirNotExisting OutDirectory, verbose: true

  pdfFiles = Dir[PdfPattern]
  unless pdfFiles.empty?
    File.delete *pdfFiles
  end

  if IsPresentation
    ensureFileNotExisting ModeFile, verbose: true
    ensureFileNotExisting ModeBeforeFile, verbose: true
    ensureFileNotExisting OutFileName, verbose: true
  end

  unless args[:all].nil?
    ensureDirNotExisting ProducedDirectory, verbose: true
  end
end
